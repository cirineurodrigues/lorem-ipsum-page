//Desenvolvido por: Cirineu Rodrigues

//Variáveis globais
let isMenuClosed = true;
let topDistance = 0;

//Função para fazer uma animação com transição de opacidade no titulo do banner
const showTitle = () => {
  const bannerTitle = document.querySelector("div.textBanner div h1"); //Pega o elemento
  bannerTitle.classList.add("bannerTitle"); //Troca a classe
};

//Função para animação de escrita no subtitulo do banner
const writeText = () => {
  const bannerSubtitle = document.querySelector("#bannerSubtitle"); //Pega o elemento pelo id
  const text = bannerSubtitle.innerHTML.split(""); //Faz um array separando as palavras por letras
  bannerSubtitle.innerHTML = ""; //Zera o conteúdo da tag

  text.forEach((letter, index) => {
    //Para cada letra
    setTimeout(() => {
      bannerSubtitle.innerHTML += letter; //Adiciona no innerHTML
    }, 75 * index); //A cada 75 milissegundos
  });
};

//Função para fazer a animação do menu hamburguer
const animateButton = () => {
  const stripe = document.querySelectorAll(".stripe"); //Retorna uma node list
  const square = ["firstStripe", "secondStripe", "thirdStripe"]; //Ids para quando o menu está com as listras na horizontal
  const x = [
    "firstStripeAnimated",
    "secondStripeAnimated",
    "thirdStripeAnimated",
  ]; //Ids para quando o menu está em X

  if (isMenuClosed) {
    //Se o menu estiver fechado
    stripe.forEach((element, index) => {
      element.id = square[index]; //Altera os ids com o array de square
    });
  } else {
    //Se o menu estiver aberto
    stripe.forEach((element, index) => {
      element.id = x[index]; //Altera os ids com o array de X
    });
  }
};

//Função para mostrar o menu
const showMenu = () => {
  const menu = document.querySelector("#mobileMenu"); //Seleciona o elemento pelo id

  if (isMenuClosed) {
    //Se o menu estiver fechado
    menu.classList.add("mobileMenuOpend"); //Muda a classe para mobileMenuOpend
    menu.classList.remove("mobileMenuClosed"); //Remove a classe mobileMenuClosed
    isMenuClosed = false; //Troca o valor da variável para false

    animateButton();
  } else {
    //Se o menu estiver aberto
    menu.classList.add("mobileMenuClosed"); //Muda a classe  para mobileMenuClosed
    menu.classList.remove("mobileMenuOpend"); //Remove a classe mobileMenuOpend
    isMenuClosed = true; //Troca o valor da variável para true

    animateButton();
  }
};

//Função para pegar a distância do topo de um elemento pelo atributo href
const getTop = (element) => {
  const getHref = element.getAttribute("href"); //Pega o valor do atributo href do elemento
  return document.querySelector(getHref).offsetTop; //Usa o hfef para selecionar o  elemento através do id
};

//Função para mudar a cor dos itens do menu deskop de acordo com a posição das sections
const changeMenuColor = () => {
  const listItems = document.querySelectorAll(".listItems");
  const elements = document.querySelectorAll(".getTop");
  //Armazena as alturas dos elementos que são referenciados no menu
  const firstTop = elements[0].offsetTop - 101;
  const secondTop = elements[1].offsetTop - 101;
  const thirdTop = elements[2].offsetTop - 101;
  let currentTop = window.scrollY; //Pega a posição atual do scroll

  if (currentTop > topDistance) {
    //Se o scroll estiver descendo
    if (currentTop >= firstTop) {
      listItems[0].classList.add("on");
    }

    if (currentTop > secondTop) {
      listItems[0].classList.remove("on");
      listItems[1].classList.add("on");
    }

    if (currentTop > thirdTop) {
      listItems[1].classList.remove("on");
      listItems[2].classList.add("on");
    }
  } else {
    //Se o scroll estiver subindo

    if (currentTop < thirdTop) {
      listItems[2].classList.remove("on");
      listItems[1].classList.add("on");
    }

    if (currentTop < secondTop) {
      listItems[1].classList.remove("on");
      listItems[0].classList.add("on");
    }

    if (currentTop < firstTop) {
      listItems[0].classList.remove("on");
    }
  }

  topDistance = currentTop; //Atualiza o valor da variável topDistance
};

//Função para fazer o scroll até determinada posição
const scrollToPosition = (top, adjustment) => {
  window.scroll({
    //Método scroll do objeto Window
    top: top - adjustment, //Faz um ajuste no top
    behavior: "smooth", //Deixa a rolagem da tela mais suave
  });
};

//Função para rolar até o topo no desktop
const scrollToTheTopDesktop = (evt) => {
  evt.preventDefault(); //Evitar os comportamentos padrões

  scrollToPosition(0, 0);
};

//Função para rolar até o topo no mobile
const scrollToTheTopMobile = (evt) => {
  evt.preventDefault(); //Evitar os comportamentos padrões

  scrollToPosition(0, 0);
  showMenu();
};

//Função para rolar até as sections do menu desktop
const menuScrollDesktop = (evt) => {
  evt.preventDefault(); //Evitar os comportamentos padrões
  const top = getTop(evt.target); //Armazena o valor de retorno da função getTop

  scrollToPosition(top, 100);
};

//Função para rolar até as sections do menu mobile
const menuScrollMobile = (evt) => {
  evt.preventDefault(); //Evitar os comportamentos padrões
  const top = getTop(evt.target); //Armazena o valor de retorno da função

  showMenu(); //Para fechar o menu depois do click
  scrollToPosition(top, 100);
};

//Função para rolar até a primeira section
const scrollToFirstSection = (evt) => {
  evt.preventDefault(); //Evitar os comportamentos padrões
  const top = getTop(evt.target); //Armazena o valor de retorno da função getTop

  scrollToPosition(top, 100);
};

//EventListeners
document.querySelector("#hamburguerMenu").addEventListener("click", showMenu); //Botão menu mobile
document
  .querySelector(".header a")
  .addEventListener("click", scrollToTheTopDesktop); //Logo do cabeçalho
document.querySelector("#up").addEventListener("click", scrollToTheTopMobile); //Up menu mobile
document
  .querySelector("#arrow")
  .addEventListener("click", scrollToFirstSection); //Seta banner
window.addEventListener("scroll", changeMenuColor);
window.addEventListener("load", showTitle);
window.addEventListener("load", writeText);

//Adiciona EventListeners a todos os elementos do menu desktop
const menuItemsDesktop = document.querySelectorAll(".menu a");
menuItemsDesktop.forEach((item) => {
  item.addEventListener("click", menuScrollDesktop);
});

//Adiciona EventListeners a todos os elementos do menu desktop
const menuItemsMobile = document.querySelectorAll(".menuMobileScroll a");
menuItemsMobile.forEach((item) => {
  item.addEventListener("click", menuScrollMobile);
});
